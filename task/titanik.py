import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df['temp'] = df['Name'].str.extract(r', (\w+\.)')

    median_ages = df.groupby('temp')['Age'].median().round().astype(int)

    missing_values = df.groupby('temp')['Age'].apply(lambda x: x.isnull().sum())

    tuple_result = [('Mr.', missing_values['Mr.'], median_ages['Mr.']),
                    ('Mrs.', missing_values['Mrs.'], median_ages['Mrs.']),
                    ('Miss.', missing_values['Miss.'], median_ages['Miss.'])]

    return tuple_result